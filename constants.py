from path import Path

#Path
BLUR_V1_CPU = Path('/home/mikedev/Programming/Projects/Cpp/BlurImg')
BLUR_V2_CPU = BLUR_V1_CPU.parent / 'BlurImgV2'
BLUR_GPU = Path('/home/mikedev/Programming/Projects/Cuda/BlurImgV2')
ROOT = Path(__file__).parent
PLOT_FOLDER = ROOT / 'plots'
TIMES_CPU_PATH = ROOT / 'times_cpu.csv'
TIMES_GPU_PATH = ROOT / 'times_gpu.csv'
SPEEDUP_CPU_PATH = ROOT / 'speedup_cpu.csv'
SPEEDUP_GPU_PATH = ROOT / 'speedup_gpu.csv'
SPEEDUP_CPU_GPU_PATH = ROOT / 'speedup_cpu_gpu.csv'

IMAGES = ['4k.ppm', 'fullhd.ppm', 'face.ppm']
BASENAMES = [imgname.split(".", 1)[0] for imgname in IMAGES]
RES_MAP_IMAGES = {'face': '108x156', 'fullhd': '1920x1080', '4k': '4320x2433'}
RES_IMAGES =  ['108x156', '1920x1080', '4320x2433']
CONVSIZES = ['3x3', '5x5', '7x7']
CPU_VERSIONS = ['Channel first', 'Channel last']
GPU_VERSIONS = ['Base', 'Tiling', 'BaseConstant', 'TilingConstant']