import matplotlib.pyplot as plt
import seaborn as sns
from itertools import chain
import numpy as np
import pandas as pd
from path import Path
from constants import *

def timefiles_gpu_with_vars():
    prefix = "ImBlurCuda"
    for suffix in ['Base', 'Tiling', 'BaseConstant', 'TilingConstant']:
        for convsize in CONVSIZES:
            for basename in BASENAMES:
                timefile = f'time_{prefix + suffix + convsize}_{basename}.txt'
                yield suffix, convsize, basename, timefile

def df_times_cpu():
    executables = ['ImBlurK3', 'ImBlurK5', 'ImBlurK7']
    df_times = pd.DataFrame(columns=['time_ms', 'res_img', 'version', 'convsize'])
    for executable in executables:
        for i, folder in enumerate([BLUR_V1_CPU, BLUR_V2_CPU]):
            version = 'Channel last' if i == 0 else 'Channel first'
            for img in BASENAMES:
                with open(folder / f'time_{executable}_{img}.txt') as f:
                    txt = f.read().split('\n')
                    times = [float(x) for x in txt if x]
                ksize = executable[-1]; ksize = ksize + 'x' + ksize
                times = np.array(times)
                times = pd.DataFrame({'time_ms': times, 'res_img': RES_MAP_IMAGES[img], 'version': version, 'convsize': ksize})
                df_times = pd.concat([df_times, times])
    df_times.to_csv('times_cpu.csv', index=False)
    return df_times

def df_times_gpu():
    data = []
    for (technique, convsize, imgfile, timefile) in timefiles_gpu_with_vars():
        print('reading ', timefile)
        with open(BLUR_GPU / timefile) as f:
            times = [float(x) for x in f.read().split('\n') if x]
        for t in times:
            data.append([technique, convsize, RES_MAP_IMAGES[imgfile], t])
    data = pd.DataFrame(data, columns=['version', 'convsize', 'res_img', 'time_ms'])
    data.to_csv('times_gpu.csv', index=False)

def df_speedup_gpu():
    data = pd.read_csv(TIMES_GPU_PATH)
    versions = {}
    for gpu_v in GPU_VERSIONS:
        data_gpu_v = data.query(f'version == "{gpu_v}"').drop(columns='version')
        versions[gpu_v] = data_gpu_v
    speedup = versions['Base'].copy()
    for gpu_v in GPU_VERSIONS:
        if gpu_v != "Base":
            newcol = 'time_ms_'+ gpu_v.lower()
            speedup[newcol] = versions['Base'].merge(versions[gpu_v], on=['convsize', 'res_img'], suffixes=('_base', '_' + gpu_v.lower()))[newcol]
            speedup['speedup_base_' + gpu_v.lower()] = speedup['time_ms'] /  speedup[newcol]
    speedup.to_csv('speedup_gpu.csv', index=False)

def df_speedup_gpu_aggr():
    data = pd.read_csv(SPEEDUP_GPU_PATH)
    speedup_cols = data.columns[data.columns.str.startswith('speedup')]
    data = data.pivot_table(index='res_img', columns='convsize', values=speedup_cols, aggfunc=['mean', 'std'])
    data.to_csv(SPEEDUP_GPU_PATH.replace('speedup_', 'speedup_aggr_'))


def df_speedup_cpu_gpu():
    if not TIMES_CPU_PATH.exists() or not TIMES_GPU_PATH.exists():
        raise IOError(f"{TIMES_CPU_PATH} or {TIMES_GPU_PATH} don't exist")
    times_cpu = pd.read_csv('times_cpu.csv')
    times_gpu = pd.read_csv('times_gpu.csv')
    speedup = times_cpu.merge(times_gpu, on=['convsize', 'res_img'], suffixes=('_cpu', '_gpu'))
    speedup['speedup'] = speedup['time_ms_cpu'] / speedup['time_ms_gpu']
    speedup.to_csv('speedup_cpu_gpu.csv', index=False)

if __name__ == "__main__":
    df_times_cpu()
    df_times_gpu()
    df_speedup_cpu_gpu()
    df_speedup_gpu()
    df_speedup_gpu_aggr()
