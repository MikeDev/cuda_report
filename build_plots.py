import matplotlib.pyplot as plt
import seaborn as sns
from itertools import chain
import numpy as np
import pandas as pd
from path import Path
from constants import *
from random import random, setstate, seed

def timefiles_gpu_with_vars():
    prefix = "ImBlurCuda"
    for suffix in GPU_VERSIONS:
        for convsize in CONVSIZES:
            for basename in BASENAMES:
                timefile = f'time_{prefix + suffix + convsize}_{basename}.txt'
                yield suffix, convsize, basename, timefile


def times_cpu(with_times=True, with_speedup=True):
    global RES_IMAGES
    df_times = pd.read_csv(TIMES_CPU_PATH)
    df_times['time_ms'] = df_times['time_ms'] / 1000
    max_time = df_times['time_ms'].max()
    for i, convsize in enumerate(CONVSIZES):
        fig, ax = plt.subplots(figsize=(6,4))
        data_conv = df_times.query(f'convsize == "{convsize}"')
        sns.barplot(x='res_img', y='time_ms', hue='version', data=data_conv,
                    order=RES_IMAGES, ax=ax)
        ax.set_title(convsize)
        ax.set_ylim(0, max_time)
        ax.set_ylabel('Time (s)')
        if i == 0:
            handles, labels = ax.get_legend_handles_labels()
            ax.legend(handles=handles, labels=['Not optimized', 'Optimized'])
        else:
            ax.get_legend().remove()
            ax.set_ylabel('')
        ax.set_xlabel('')
        plt.savefig(PLOT_FOLDER / f'cpu_time_{convsize}.png')
        plt.close()


def speedup_cpu():
    df_times = pd.read_csv(TIMES_CPU_PATH)
    df_times_2 = df_times.groupby(['res_img', 'convsize', 'version'])[['time_ms']].mean().reset_index()
    ch_first = df_times_2.query('version == "Channel first"').drop(columns='version').set_index(['res_img', 'convsize'])
    ch_last = df_times_2.query('version == "Channel last"').drop(columns='version').set_index(['res_img', 'convsize'])
    speedup = ch_last / ch_first
    speedup = speedup.reset_index()
    speedup.to_csv(SPEEDUP_CPU_PATH, index=False)
    fig, ax = plt.subplots(figsize=(9,7))
    sns.barplot('res_img', 'time_ms', data=speedup, hue='convsize', ax=ax)
    sns.pointplot('res_img', 'time_ms', data=speedup, hue='convsize', ax=ax, legend=False)
    for line in ax.lines:
        line.set_linestyle("--")
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles=handles[-len(CONVSIZES):], labels=labels[-len(CONVSIZES):]) 
    plt.title('Speedup between memory layouts ("channel last" over "channel first")')
    plt.xlabel('Resolution')
    plt.ylabel('Speedup')
    plt.savefig(PLOT_FOLDER / 'cpu_speedup.png')


def fix_heatmap():
    b, t = plt.ylim() # discover the values for bottom and top
    b += 0.5 # Add 0.5 to the bottom
    t -= 0.5 # Subtract 0.5 from the top
    plt.ylim(b, t) # update the ylim(bottom, top) values

def fix_heatmap_ax(ax):
    b, t = ax.get_ylim() # discover the values for bottom and top
    b += 0.5 # Add 0.5 to the bottom
    t -= 0.5 # Subtract 0.5 from the top
    ax.set_ylim(b, t) # update the ylim(bottom, top) values


def pixel_loads_cuda_block():
    data = [[1,2,3,2,1],
            [2,6,6,6,2], 
            [3,6,9,6,3],
            [2,6,6,6,2],
            [1,2,3,2,1]]
    data = np.array(data)
    sns.heatmap(data, annot=True, cbar_kws=dict(ticks=np.arange(1,10)))
    fix_heatmap()
    plt.title('Pixel usage with a 3x3 block size and mask size')
    plt.savefig(PLOT_FOLDER / 'pixel_usage_block.png')


def tiling_matrix_loadings():
    data = [[1,2,1,2,3,4,5,4,5],
            [6,7,6,7,8,9,10,9,10],
            [1,2,1,2,3,4,5,4,5],
            [6,7,6,7,8,9,10,9,10],
            [11,12,11,12,13,14,15,14,15],
            [16,17,16,17,18,19,20,19,20],
            [21,22,21,22,23,24,25,24,25],
            [16,17,16,17,18,19,20,19,20],
            [21,22,21,22,23,24,25,24,25]]
    data = np.array(data)
    seed(13)
    cmap = [(random(), random(), random()) for i in range(1, 26)]
    sns.heatmap(data, annot=True, cmap=cmap, cbar=False)
    fix_heatmap()
    plt.title('Load per thread id in a 5x5 CUDA block and 5x5 mask size')
    plt.savefig(PLOT_FOLDER / 'pixel_load_tiling.png')
    plt.close()
    nloads = np.zeros((5,5))
    for i in range(5):
        for j in range(5):
            i_array = i * 5 + (j + 1)
            nloads[i,j] = (data == i_array).sum()
    sns.heatmap(nloads, annot=True, cbar=False)
    fix_heatmap()
    plt.title('#Loads per thread ID in a 5x5 CUDA block and mask size')
    plt.savefig(PLOT_FOLDER / 'pixel_numloads_tiling.png')
    plt.close()




def data_locality():
    rgb_palette = sns.xkcd_palette(['red', 'green', 'blue'])
    data1 = [(1,2,3) for i in range(10)]
    data1 = list(chain(*data1))
    data2 = [1] * 10 + [2] * 10 + [3] * 10
    data1 = np.array(data1).reshape((10,3))
    data2 = np.array(data2).reshape((10,3))
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(14, 8))
    ticks = np.arange(4)
    sns.heatmap(data1, ax=ax1, cmap=rgb_palette, cbar=False, linewidths=0.5, linecolor='black')
    sns.heatmap(data2, ax=ax2, cmap=rgb_palette, cbar=False, linewidths=0.5, linecolor='black')
    fix_heatmap_ax(ax1)
    fix_heatmap_ax(ax2)
    ax1.set_title('RGBRGBRGB format (channel last)')
    ax2.set_title('RRRGGGBBB format (channel first)')
    plt.savefig(PLOT_FOLDER / 'data_locality.png')

def times_gpu():
    data = pd.read_csv(TIMES_GPU_PATH)
    data['time_ms'] = data['time_ms'] / 1000
    max_time = data['time_ms'].max()
    for i, convsize in enumerate(CONVSIZES):
        fig, ax = plt.subplots(figsize=(6,4))
        data_conv = data.query(f'convsize == "{convsize}"')
        sns.barplot(x='res_img', y='time_ms', hue='version', data=data_conv,
                    order=RES_IMAGES, ax=ax)
        sns.lineplot(x='res_img', y='time_ms', hue='version', data=data_conv, ax=ax)

        ax.set_ylim(0, max_time)
        ax.set_title(convsize)
        ax.set_ylabel('Time (s)')
            # handles, labels = ax.get_legend_handles_labels()
        if i != 0:
            ax.get_legend().remove()
        else:
            handles, labels = ax.get_legend_handles_labels()
            ax.legend(handles=handles[-len(GPU_VERSIONS):], labels=labels[-len(GPU_VERSIONS):]) 
        ax.set_xlabel('')
        plt.savefig(PLOT_FOLDER / f'gpu_time_{convsize}.png')
        plt.close()

def speedup_gpu():
    data = pd.read_csv(SPEEDUP_GPU_PATH)
    max_value = data.loc[:, data.columns.str.startswith('speedup')].max().max()
    for gpu_v in GPU_VERSIONS:
        if gpu_v != 'Base':
            fig, ax = plt.subplots(figsize=(6, 4))
            speedup_col = 'speedup_base_' + gpu_v.lower()
            sns.barplot('res_img', speedup_col, data=data, hue='convsize', ax=ax, order=RES_IMAGES)
            ax.set_ylim(0, max_value)
            ax.set_title(f'Speedup between Base version and {gpu_v}')
            plt.savefig(PLOT_FOLDER / f'speedup_gpu_{gpu_v.lower()}.png')
            plt.close()

def speedup_cpu_gpu():
    data = pd.read_csv(SPEEDUP_CPU_GPU_PATH)
    fig, axs = plt.subplots(ncols=len(CPU_VERSIONS), nrows=len(CONVSIZES), figsize=(3 * len(CONVSIZES), 5 * len(CPU_VERSIONS)))
    for i, convsize in enumerate(CONVSIZES):
        data_convsize = data.query(f'convsize == "{convsize}"')
        for j, cpu_v in enumerate(reversed(CPU_VERSIONS)):
            ax = axs[i, j]
            data_cpu_v = data_convsize.query(f'version_cpu == "{cpu_v}"')
            if j == 0:
                cpu_v = 'Cpu not optimized'
            elif j == 1:
                cpu_v = 'Cpu optimized'
            sns.barplot('res_img', 'speedup', data=data_cpu_v, hue='version_gpu', ax=ax, order=RES_IMAGES)
            sns.pointplot('res_img', 'speedup', data=data_cpu_v, hue='version_gpu', ax=ax, order=RES_IMAGES)
            for line in ax.lines:
                line.set_linestyle("--")
            handles, labels = ax.get_legend_handles_labels()
            if i == 0 and j == 0:
                ax.legend(handles=handles[-len(GPU_VERSIONS):], labels=labels[-len(GPU_VERSIONS):]) 
            else:
                ax.get_legend().remove() 
            if j == 0:
                ax.set_ylabel(convsize)
            else:
                ax.set_ylabel('')
            if i == 0:
                ax.set_title(cpu_v)
            else:
                ax.set_title('')
            ax.set_xlabel('')
    fig.suptitle(f'Speedup matrix between CPU and GPU')
    fig.savefig(PLOT_FOLDER / f'speedup_cpu_gpu.png')
    plt.close()


def main():
    sns.set_style('whitegrid')
    if not PLOT_FOLDER.exists():
        PLOT_FOLDER.mkdir()
    # data_locality()
    # times_cpu()
    # plt.close()
    # times_gpu()
    # plt.close()
    speedup_gpu()
    plt.close()
    # speedup_cpu_gpu()
    # tiling_matrix_loadings()

if __name__ == '__main__':
    main()
